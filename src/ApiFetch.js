import React, { useState, useEffect } from "react";
import axios from "axios";
import Table from "./Table";
import "./App.css";

const ApiFetch = () => {
  const [articles, setArticles] = useState([]);
  const columns = [
    {
      Header: "ID",
      accessor: "id",
    },
    {
      Header: "Country Code",
      accessor: "code",
    },
    {
      Header: "Country",
      accessor: "country",
    },
  ]

  useEffect(() => {
    axios
      .get("http://instapreps.com/api/country_code")
      .then((res) => {
        console.log(res);
        let tempArr = [];
        for (let i = 0; i < 26; i++) {
          tempArr.push({
            id: res.data.data[i].id,
            code: res.data.data[i].code,
            country: res.data.data[i].country,
          });
        }
        setArticles(tempArr);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div>
      <h1 className="sub">DATA of Countries</h1>

      <div className="table">
        <Table columns={columns} data={articles} />

        {/* <h2 key={el.id}>{el.id}   {el.country}  {el.code} </h2> */}
      </div>
    </div>
  );
};

export default ApiFetch;
