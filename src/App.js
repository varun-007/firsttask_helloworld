import logo from "./masterlogopng.png";
import "./App.css";
import Typewriter from "typewriter-effect";
import ApiFetch from "./ApiFetch";

function App() {
  return (
    <div className="App">
      <div className="logo">
        <img src={logo} className="App-logo" alt="logo" />
        <p className="text">This is my First Task at Seven Classes</p>

        <div className="sub">
          <Typewriter
            options={{
              strings: [
                "Hello",
                "World",
                "I am Varun Bhardwaj",
                "React Intern",
              ],
              autoStart: true,
              loop: true,
            }}
          />
        </div>
        <ApiFetch />
      </div>
    </div>
  );
}

export default App;
